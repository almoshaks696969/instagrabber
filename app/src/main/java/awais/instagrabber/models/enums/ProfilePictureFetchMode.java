package awais.instagrabber.models.enums;

public enum ProfilePictureFetchMode {
    INSTADP,
    INSTA_STALKER,
    INSTAFULLSIZE,
}